import simple_draw as sd

sd.resolution = (1300, 700)


def chess(row, column):
    for y in range(100, (row * 100 + 100), 200):
        for x in range(100, (column * 100 + 100), 200):
            point = sd.get_point(x, y)
            sd.square(point, side=100, color=sd.COLOR_WHITE)
    for y in range(200, row * 100, 200):
        for x in range(200, column * 100, 200):
            point_1 = sd.get_point(x, y)
            sd.square(point_1, side=100, color=sd.COLOR_WHITE)
    for y in range(200, (row * 100 + 100), 200):
        for x in range(100, (column * 100 + 100), 200):
            point = sd.get_point(x, y)
            sd.square(point, side=100, color=sd.COLOR_BLACK)
    for y in range(100, (row * 100 + 200), 200):
        for x in range(200, column * 100, 200):
            point_1 = sd.get_point(x, y)
            sd.square(point_1, side=100, color=sd.COLOR_BLACK)


chess(5,3)
sd.pause()
